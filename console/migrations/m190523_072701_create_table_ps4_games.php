<?php

use yii\db\Migration;

/**
 * Class m190523_072701_create_table_ps4
 */
class m190523_072701_create_table_ps4_games extends Migration
{
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%ps4_games}}', [
            'id' => $this->primaryKey(),
            'ps4_id' => $this->string()->notNull(),
            'name' => $this->string()->notNull(),
            'description' => $this->getDb()->getSchema()->createColumnSchemaBuilder('longtext'),
            'prices' => $this->json(),
            'genre' => $this->json(),
            'score' => $this->float()->defaultValue(0),
            'score_count' => $this->integer()->defaultValue(0),
            'created_at' => $this->integer()->notNull(),
            'updated_at' => $this->integer()->notNull(),
        ], $tableOptions);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%games_ps4}}');

        return true;
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m190523_072701_create_table_ps4 cannot be reverted.\n";

        return false;
    }
    */
}
