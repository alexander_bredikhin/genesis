1) docker-compose up -d --build
2) Run migration in your app container
3) Run queues yii queue/listen
4) 

Accept -> application/json
Content-Type -> application/json
POST /api/v1/ps4

{
        "ps4_id": "EP1004-CUSA08519_00-REDEMPTION000002",
        "name": "Red Dead Redemption 2",
        "description": "Игра Red Dead Redemption 2 от создателей GTA V и Red Dead Redemption – это грандиозное повествование о жизни в Америке на заре современной эпохи.1 игрокСетевая игра: 2-32 чел. Для доступа к онлайн мультиплееру в полной версии игры необходима подписка PlayStation®PlusНе менее 105ГбDUALSHOCK®4 с функцией вибрацииПоддерживается дистанционное воспроизведениеPAL HD 720p,1080i,1080pЗагрузка осуществляется в соответствии с Условиями обслуживания PlayStation Network, Условиями использования программ и любыми другими применимыми дополнительными документами. Если вы не согласны выполнять условия, не загружайте материалы. Дополнительная информация приведена в Условиях обслуживания. Разовая лицензионная плата за право загрузки на несколько систем PS4. Вход в PlayStation Network не требуется при использовании на вашей основной системе PS4, но необходим при использовании на других системах PS4.Перед использованием продукта ознакомьтесь с «Мерами предосторожности», важными для вашего здоровья.Библиотечные программы ©Sony Interactive Entertainment Inc., эксклюзивно лицензированы Sony Interactive Entertainment Europe. Действуют Условия использования программ. Полный текст Условий использования см. на сайте ru.playstation.com/legal. License rockstargames.com/eula terms rockstargames.com/socialclub. Non-transferable access to special features such as exclusive/unlockable/downloadable/online content/services/functions, multiplayer or bonus content, may require single-use serial code, additional fee, and/or online account registration(varies 13+). Special features may require internet, may not be available to all users or at all times, and may, on 30 days notice, be terminated/modified/offered under different terms. Violation of EULA/Terms/other policies may result in restriction/termination of access to game or online account. Customer/tech support at rockstargames.com/support. This videogame is fictional. Unauthorized copying, reverse engineering, transmission, public performance, rental, pay for play, or circumvention is strictly prohibited. Take-Two owns marks/logos/copyrights. All rights reserved",
        "prices": {
            "non-plus-user": {
                "price": 199900
            },
            "plus-user": {
                "price": 199900
            }
        },
        "score": "4.69",
        "score_count": "19417",
        "genre": [
            "Боевик",
            "Приключения"
        ]
}