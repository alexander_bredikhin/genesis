<?php

declare(strict_types=1);


namespace application\models\ps4;


use yii\elasticsearch\ActiveRecord;


/**
 *
 * @property string $name
 * @property string $description
 */
class Ps4GameElastic extends ActiveRecord
{
    /**
     * @return array
     */
    public static function mapping(): array
    {
        return [
            static::type() => [
                'properties' => [
                    'name' => ['type' => 'string'],
                    'description' => ['type' => 'string'],
                ],
            ],
        ];
    }

    /**
     * @return array
     */
    public function attributes(): array
    {
        return ['name', 'description'];
    }

    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [
                ['name', 'description'],
                'string',
            ],
        ];
    }
}