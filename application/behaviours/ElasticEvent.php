<?php

declare(strict_types=1);

namespace application\behaviours;


use application\entity\Ps4ElasticEntity;
use yii\base\Behavior;
use yii\queue\Queue;

class ElasticEvent extends Behavior
{

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            Queue::EVENT_AFTER_EXEC => 'elastic',
        ];
    }


    /**
     * @param $event
     */
    public function elastic($event): void
    {
        $getPs4Game = $event->job->getPs4Game();
        $ps4ElasticEntity = new Ps4ElasticEntity();
        $createNewPs4GameElasticRecord = $ps4ElasticEntity->createPs4ElasticRecord($getPs4Game);
        $ps4ElasticEntity->save($createNewPs4GameElasticRecord);
    }

}