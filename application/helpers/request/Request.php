<?php

declare(strict_types=1);


namespace application\helpers\request;

use yii\helpers\ArrayHelper;
use yii\helpers\StringHelper;

class Request implements RequestInterface
{

    private $request;

    /**
     * Request constructor.
     * @param \yii\web\Request $request
     */
    public function __construct(\yii\web\Request $request)
    {
        $this->request = $request;
    }

    /**
     * @param $key
     * @return mixed
     */
    public function __get($key)
    {
        $requestData = $this->getRequest();
        if ($this->isJson($this->request->getRawBody())) {
            return ArrayHelper::getValue($this->jsonToArray(), $key);
        }
        if ($this->hasKey($key)) {
            return ArrayHelper::getValue($requestData, $key);
        }
    }

    public function __set($name, $value)
    {
        // TODO: Implement __set() method.
    }

    public function __isset($name)
    {
        // TODO: Implement __isset() method.
    }

    /**
     * @return \yii\web\Request
     */
    public function yiiRequest(): \yii\web\Request
    {
        return $this->request;
    }

    /**
     * @return array|mixed
     */
    public function getRequest()
    {
        $getData = [];
        if ($this->request->isPost) {
            $getData = $this->request->post();
        } elseif ($this->request->isGet) {
            $getData = $this->request->get();
        } elseif ($this->request->isPatch || $this->request->isPut) {
            $getData = $this->request->post();
        } elseif ($this->request->isAjax) {
            $getData = $this->request->post();
        }

        return $getData;
    }

    /**
     * @param $keys
     * @return array
     */
    public function getByKeys($keys): array
    {
        $result = [];
        $getData = $this->getRequest();
        foreach (is_array($keys) ? $keys : func_get_args() as $key) {
            if (($pos = strrpos($key, '.')) !== false) {
                $keys = substr($key, $pos + 1);
                $value = ArrayHelper::getValue($getData, $key);
                if (isset($value) && !empty($value)) {
                    $result[$keys] = $value;
                }
            }
            if ($this->hasKey($key)) {
                $result[$key] = $getData[$key];
            }
        }

        return $result;
    }

    /**
     * @param bool $asArray
     * @return array
     * @throws \Exception
     */
    public function jsonToArray(bool $asArray = true): array
    {
        $data = $this->request->getRawBody();
        $result = [];
        if (!$this->isJson($data)) {
            throw new \Exception('Invalid JSON data.');
        }
        if (isset($data) && !empty($data)) {
            $result = json_decode($data, $asArray);
        }

        return $result;
    }

    /**
     * @param $keys
     * @return array
     */
    public function exceptKeys($keys): array
    {
        $result = [];
        $getData = $this->getRequest();

        foreach (is_array($keys) ? $keys : func_get_args() as $key) {
            if (array_key_exists($key, $getData)) {
                unset($getData[$key]);
            }
        }
        if (!empty($getData)) {
            $result = $getData;
        }

        return $result;
    }

    /**
     * @return string
     */
    public function getBearer(): string
    {
        $result = '';
        $getHeader = $this->request->headers->get('Authorization');
        if (isset($getHeader)) {
            if (StringHelper::startsWith($getHeader, 'Bearer ')) {
                $result = mb_substr($getHeader, 7, null, 'UTF-8');
            }
        }

        return $result;
    }

    /**
     * @param key
     * @return bool
     */
    public function hasKey($keys): bool
    {
        $getData = $this->getRequest();
        foreach (is_array($keys) ? $keys : func_get_args() as $key) {
            if (!array_key_exists($key, $getData)) {
                return false;
            }
        }

        return true;
    }

    /**
     * @param string $string
     * @return bool
     */
    public function isJson($string): bool
    {
        return (is_string($string) && is_array(json_decode($string,
            true)) && (json_last_error() === JSON_ERROR_NONE));
    }

}