<?php

declare(strict_types=1);

namespace application\helpers\request;


interface RequestInterface
{
    public function yiiRequest(): \yii\web\Request;

    /**
     * @return array|mixed
     */
    public function getRequest();

    /**
     * @param $keys
     * @return array
     */
    public function getByKeys($keys): array;

    /**
     * @param bool $asArray
     * @return array
     */
    public function jsonToArray(bool $asArray = true): array;

    /**
     * @param $keys
     * @return array
     */
    public function exceptKeys($keys): array;

    /**
     * @return string
     */
    public function getBearer(): string;

    /**
     * @param key
     * @return bool
     */
    public function hasKey($keys): bool;


    /**
     * @param string $string
     * @return bool
     */
    public function isJson(string $string): bool;

}