<?php
return [

    [
        'class' => \yii\rest\UrlRule::class,
        'controller' => 'v1/ps4',
        'pluralize' => false,
        'except' => ['delete', 'update', 'view'],

    ],
];