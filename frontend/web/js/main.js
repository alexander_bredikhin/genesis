(function () {

    $('.find-by-name').on('keypress', function (e) {
        if (e.which == 13) {
            var gameName = $(this).val();
            sendAjaxRequest(gameName, 'desc')
        }
    });


    $('.sort').on('click', function (e) {
        e.preventDefault();
        var sort = $(this).data('sort');

        if (sort == 'asc') {
            $(this).data('sort', 'desc');
        } else {
            $(this).data('sort', 'asc');
        }
        var gameName = $('.find-by-name').val();
        sendAjaxRequest(gameName, sort)
    })

})();

function sendAjaxRequest(gameName, sort) {
    $.ajax({
        url: '/site/find-game-by-name',
        method: 'POST',
        data: {
            sort: sort,
            name: gameName
        },
        success: function (data) {
            $('#table').html(data);
        }
    });
}










