<?php
return [
    'cache' => [
        'class' => \yii\redis\Cache::class,
        'redis' => [
            'hostname' => 'redis',
            'port' => 6379,
            'database' => 0,
        ],
    ],
    'db' => [
        'class' => 'yii\db\Connection',
        'dsn' => 'mysql:host=genesis_mysql_1;dbname=genesis',
        'username' => 'user',
        'password' => 'user',
        'charset' => 'utf8',
    ],
    'redis' => [
        'class' => 'yii\redis\Connection',
        'hostname' => 'redis',
        'port' => 6379,
        'database' => 0,
    ],
    'queue' => [
        'class' => \yii\queue\redis\Queue::class,
        'as log' => \yii\queue\LogBehavior::class,
        'as elastic' => \application\behaviours\ElasticEvent::class
    ],
    'elasticsearch' => [
        'class' => 'yii\elasticsearch\Connection',
         'autodetectCluster' => false,

        'nodes' => [
            ['http_address' => 'elastic:9200'],
        ],
    ],
];