<?php

declare(strict_types=1);


namespace application\entity;

use application\helpers\request\RequestInterface;
use application\models\ps4\Ps4Games;
use yii\web\BadRequestHttpException;


class Ps4Entity
{
    /**
     * @param RequestInterface $request
     * @return Ps4Games
     */
    public function create(RequestInterface $request): Ps4Games
    {
        $createNewPs4Game = new Ps4Games([
            'name' => $request->name,
            'genre' => $request->genre,
            'description' => $request->description,
            'score' => $request->score,
            'ps4_id' => $request->ps4_id,
            'score_count' => $request->score_count,
            'prices' => $request->prices,
        ]);

        return $createNewPs4Game;
    }

    /**
     * @param Ps4Games $ps4Games
     */
    public function save(Ps4Games $ps4Games): void
    {
        if (!$ps4Games->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }
}