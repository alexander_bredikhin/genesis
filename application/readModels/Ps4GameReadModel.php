<?php

declare(strict_types=1);

namespace application\readModels;


use application\models\ps4\Ps4Games;

class Ps4GameReadModel
{

    /**
     * @param array $ps4Ids
     * @param int $sortType
     * @return array
     */
    public static function getPs4GameByIds(array $ps4Ids, int $sortType = SORT_ASC): array
    {
        return Ps4Games::find()
            ->where(['in', 'id', $ps4Ids])
            ->orderBy(['name' => $sortType])
            ->all();
    }
}