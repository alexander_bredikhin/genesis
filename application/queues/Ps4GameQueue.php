<?php

declare(strict_types=1);


namespace application\queues;


use application\entity\Ps4Entity;
use application\models\ps4\Ps4Games;
use yii\base\BaseObject;
use yii\queue\JobInterface;

class Ps4GameQueue extends BaseObject implements JobInterface
{
    public $formData;
    private $ps4Game;


    /**
     * @param \yii\queue\Queue $queue
     */
    public function execute($queue): void
    {
        $entity = new Ps4Entity();
        $this->ps4Game = $entity->create($this->formData);
        $entity->save($this->ps4Game);
    }


    /**
     * @return Ps4Games
     */
    public function getPs4Game(): Ps4Games
    {
        return $this->ps4Game;
    }
}