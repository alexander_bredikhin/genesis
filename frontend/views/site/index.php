<?php

/* @var $this yii\web\View */

$this->title = 'Genesis';
?>
<div class="site-index">
    <div class="col-sm-8">
        <div>
            <label for="find-by-name">Поиск по названию</label>
            <input type="text" class="find-by-name">
            <a href="#" class="sort" data-sort='asc'><i class="fas fa-arrows-alt-v"></i></a
            <div class="new-prod"></div>
        </div>
        <div id="table" style="margin-top: 20px; margin-bottom: 20px">
        </div>
    </div>
</div>
