<?php

declare(strict_types=1);


namespace application\forms;

use yii\base\Model;

class Ps4Form extends Model
{

    public $ps4_id;
    public $name;
    public $description;
    public $prices;
    public $score;
    public $score_count;
    public $genre;


    /**
     * @return array
     */
    public function rules(): array
    {
        return [
            [['ps4_id', 'name'], 'required'],
            [['description'], 'string'],
            [['prices','genre'], 'isArray'],
            [['score'], 'number'],
            [['score_count'], 'integer'],
            [['ps4_id', 'name'], 'string', 'max' => 255],
        ];
    }


    /**
     * @param $attr
     * @param $params
     * @param $validator
     */
    public function isArray($attr, $params, $validator): void
    {
        if (!is_array($this->$attr)) {
            $validator->addError($this, $attr, 'The {attribute} must be array ');
        }

    }

}