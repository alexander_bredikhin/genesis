<?php

namespace application\models\db;


/**
 * This is the model class for table "ps4_games".
 *
 * @property int $id
 * @property string $ps4_id
 * @property string $name
 * @property string $description
 * @property array $prices
 * @property array $genre
 * @property double $score
 * @property int $score_count
 * @property int $created_at
 * @property int $updated_at
 */
class Ps4Games extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ps4_games';
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'ps4_id' => 'Ps4 ID',
            'name' => 'Name',
            'description' => 'Description',
            'prices' => 'Prices',
            'genre' => 'Genre',
            'score' => 'Score',
            'score_count' => 'Score Count',
            'created_at' => 'Created At',
            'updated_at' => 'Updated At',
        ];
    }
}
