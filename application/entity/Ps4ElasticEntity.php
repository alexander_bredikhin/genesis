<?php

declare(strict_types=1);

namespace application\entity;


use application\models\ps4\Ps4GameElastic;
use application\models\ps4\Ps4Games;

class Ps4ElasticEntity
{
    /**
     * @param Ps4Games $ps4Game
     * @return Ps4GameElastic
     */
    public function createPs4ElasticRecord(Ps4Games $ps4Game): Ps4GameElastic
    {
        $newPs4Elastic = new Ps4GameElastic([
            'primaryKey' => $ps4Game->id,
            'name' => $ps4Game->name,
            'description' => $ps4Game->description,
        ]);

        return $newPs4Elastic;
    }

    /**
     * @param Ps4GameElastic $ps4GameElastic
     * @throws \Exception
     */
    public function save(Ps4GameElastic $ps4GameElastic): void
    {
        if (!$ps4GameElastic->save()) {
            throw new \RuntimeException('Saving error.');
        }
    }
}