<?php

declare(strict_types=1);


namespace application\behaviours;


use yii\base\Behavior;
use yii\db\ActiveRecord;
use yii\helpers\Json;

class JsonBehaviour extends Behavior
{
    public $attributes = [];

    /**
     * @return array
     */
    public function events(): array
    {
        return [
            ActiveRecord::EVENT_BEFORE_INSERT => 'encode',
            ActiveRecord::EVENT_BEFORE_UPDATE => 'encode',
            ActiveRecord::EVENT_AFTER_FIND => 'decode',
        ];
    }


    /**
     *
     */
    public function encode(): void
    {
        if (is_array($this->attributes)) {
            foreach ($this->attributes as $attribute) {
                $this->owner->$attribute = Json::encode($this->owner->$attribute);
            }
        }
    }

    /**
     *
     */
    public function decode(): void
    {
        if (is_array($this->attributes)) {
            foreach ($this->attributes as $attribute) {
                $this->owner->$attribute = Json::decode($this->owner->$attribute);
            }
        }
    }
}