<?php

declare(strict_types=1);

namespace application\readModels;


use application\models\ps4\Ps4GameElastic;

class Ps4GameElasticReadModel
{
    /**
     * @param string $text
     * @return Ps4GameElastic[]|array
     */
    public static function getGamesIdsByName(string $text): array
    {
        $params = [
            'multi_match' => [
                'fields' => ['name'],
                'query' => $text,
                'fuzziness' => 'AUTO',

            ],
        ];

        return Ps4GameElastic::find()->query($params)->limit(10000)->asArray()->all();
    }
}