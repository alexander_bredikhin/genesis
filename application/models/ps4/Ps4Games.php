<?php

declare(strict_types=1);


namespace application\models\ps4;


use application\behaviours\JsonBehaviour;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;

class Ps4Games extends \application\models\db\Ps4Games
{
    /**
     * @return array
     */
    public function behaviors():array
    {
        $behaviors = parent::behaviors();
        $behaviors['timeStamp'] =
            [
                'class' => TimestampBehavior::class,
                'createdAtAttribute' => 'created_at',
                'attributes' => [
                    ActiveRecord::EVENT_BEFORE_INSERT => ['created_at', 'updated_at'],
                    ActiveRecord::EVENT_BEFORE_UPDATE => ['updated_at'],
                ],
                'value' => time(),
            ];

        $behaviors['json'] = [
            'class' => JsonBehaviour::class,
            'attributes' => ['genre', 'prices'],

        ];


        return $behaviors;
    }


    /**
     * @return array
     */
    public function rules():array
    {
        return [
            [['ps4_id', 'name', 'genre', 'prices'], 'required'],
            [['description'], 'string'],

            [['score'], 'number'],
            [['created_at', 'updated_at'], 'safe'],
            [['score_count', 'created_at', 'updated_at'], 'integer'],
            [['ps4_id', 'name'], 'string', 'max' => 255],
        ];
    }


}