<?php

declare(strict_types=1);

namespace application\bootstrap;

use application\helpers\request\Request;
use application\helpers\request\RequestInterface;
use yii\base\BootstrapInterface;

class RequestBootstrap implements BootstrapInterface
{
    public function bootstrap($app): void
    {
        $container = \Yii::$container;
        $container->setSingleton(RequestInterface::class, Request::class);
    }

}