<?php

declare(strict_types=1);


namespace api\controllers\v1;

use api\controllers\BaseController;
use application\forms\Ps4Form;
use application\helpers\request\RequestInterface;
use application\queues\Ps4GameQueue;
use Yii;
use yii\base\Module;
use yii\web\BadRequestHttpException;

class Ps4Controller extends BaseController
{
    private $request;

    public function __construct($id, Module $module, RequestInterface $request, array $config = [])
    {
        $this->request = $request;
        parent::__construct($id, $module, $config);
    }

    /**
     * @param Ps4Form $form
     * @return array
     * @throws BadRequestHttpException
     */
    public function actionCreate(Ps4Form $form): array
    {
        $response = [];
        try {
            if ($form->load($this->request->jsonToArray(), '') && $form->validate()) {
              Yii::$app->queue->push(new Ps4GameQueue([
                    'formData' => $this->request,
                ]));
                $response = ['success' => true];
            }
            if ($form->hasErrors()) {
                $response = ['success' => false, 'message' => ['errors' => $form->getErrors()]];
            }
        } catch (\Exception $e) {
            throw new BadRequestHttpException();
        }

        return $response;
    }
}