<table class="table">
    <thead>
    <tr>
        <th scope="col">id</th>
        <th scope="col">ps4_id</th>
        <th scope="col">Name</th>
        <th scope="col">Description</th>
        <th scope="col">Genre</th>
    </tr>
    </thead>
    <?php if (!empty($ps4Games)) : ?>
        <tbody>
        <?php foreach ($ps4Games as $ps4Game): ?>
            <tr>
                <th scope="row"><?=$ps4Game->id ?></th>
                <td><?= $ps4Game->ps4_id ?></td>
                <td><?= $ps4Game->name ?></td>
                <td><?= $ps4Game->description ?></td>
                <td><?= implode(' , ',$ps4Game->genre) ?></td>

            </tr>
        <?php endforeach; ?>
        </tbody>
    <?php endif; ?>
</table>