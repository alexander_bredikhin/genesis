<?php

namespace frontend\controllers;

use application\helpers\request\Request;
use application\readModels\Ps4GameElasticReadModel;
use application\readModels\Ps4GameReadModel;
use application\traits\ControllerTrait;
use yii\helpers\ArrayHelper;
use yii\web\Controller;


/**
 * Site controller
 */
class SiteController extends Controller
{
    use ControllerTrait;


    public function actionIndex()
    {
        return $this->render('index');
    }


    public function actionFindGameByName(Request $request)
    {
        $getGameIdsFormElastic = ArrayHelper::getColumn(Ps4GameElasticReadModel::getGamesIdsByName($request->name),
            '_id');

        $ps4Games = Ps4GameReadModel::getPs4GameByIds($getGameIdsFormElastic, $request->sort === 'asc' ? SORT_ASC : SORT_DESC);

        return $this->renderPartial('searching', ['ps4Games' => $ps4Games]);
    }

}
