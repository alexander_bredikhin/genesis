<?php

declare(strict_types=1);

namespace api\controllers;


use application\traits\ControllerTrait;
use yii\rest\Controller;

class BaseController extends Controller
{
    use ControllerTrait;
}